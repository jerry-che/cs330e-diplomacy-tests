#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    
    def test_read_1(self):
        s = "A Madrid Hold\n"
        [i, j, k] = diplomacy_read(s)
        self.assertEqual(i,  "A")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, ["Hold"])

    def test_read_2(self):
        s = "C Paris Attack B\n"
        [i, j, k] = diplomacy_read(s)
        self.assertEqual(i, "C")
        self.assertEqual(j, "Paris")
        self.assertEqual(k, ["Attack", "B"])

    def test_read_3(self):
        s = "D Houston Move Austin\n"
        [i, j, k] = diplomacy_read(s)
        self.assertEqual(i, "D")
        self.assertEqual(j, "Houston")
        self.assertEqual(k, ["Move", "Austin"])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", ["Hold"]], ["C", "Paris", ["Support", "A"]]])
        self.assertEqual(v, [("A", "Madrid"), ("C", "Paris")])

    def test_eval_2(self):
        v = diplomacy_eval([["A", "Madrid", ["Hold"]], ["B", "Barcelona", ["Move", "Madrid"]]])
        self.assertEqual(v, [("A", "[dead]"), ("B", "[dead]")])
    
    def test_eval_3(self):
        v = diplomacy_eval([["A", "Madrid", ["Hold"]], ["B", "Barcelona", ["Move", "Madrid"]], ["C", "London", ["Support", "B"]], ["D", "Austin", ["Move", "London"]]])
        self.assertEqual(v, [("A", "[dead]"), ("B", "Madrid"), ("C", "[dead]"), ("D", "[dead]")])


    # -----
    # print
    # -----
    
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, "A", "[dead]")
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, "B", "Madrid")
        self.assertEqual(w.getvalue(), "B Madrid\n")

    def test_print_3(self):
        w=StringIO()
        diplomacy_print(w, "C", "Paris")
        self.assertEqual(w.getvalue(), "C Paris\n")
        
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nC Paris Support A\n")
        w = StringIO()
        v = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nC Paris\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Move Paris\n B Paris Hold\n C London Support B\n")
        w = StringIO()
        v = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Paris\nC London\n")
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        v = diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
    
        
# ----
# main
# ----

if __name__ == "__main__":
    main()
